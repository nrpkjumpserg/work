﻿var fileApp = angular.module('fileApp', []);

fileApp.controller('FileListCtrl', function ($scope, $http) {
    $scope.ItemFiles = undefined;
    $scope.ItemFolders = undefined;
    $scope.CurrentDirectory = undefined;
    $scope.less = undefined;
    $scope.moreless = undefined;
    $scope.more = undefined;
    $http.get("/api/Directory").success(function (data) {
        $scope.ItemFolders = data.Lfolders;
        $scope.ItemFiles = data.Lfiles;
        $scope.CurrentDirectory = data.CurrentDirectory;
        $scope.less = data.LessTen;
        $scope.moreless = data.TenToFifty;
        $scope.more = data.MoreHundred;
    }).error(function (data) {
        $scope.Item = "Oops... something went wrong";
        $scope.working = false;        
    });
    $scope.showDirectory = function (path) {
        $http.get("/api/Directory?path=" + path).success(function (data) {
            $scope.ItemFolders = data.Lfolders;
            $scope.ItemFiles = data.Lfiles;
            $scope.CurrentDirectory = data.CurrentDirectory;
            $scope.less = data.LessTen;
            $scope.moreless = data.TenToFifty;
            $scope.more = data.MoreHundred;
        });
    };
});

