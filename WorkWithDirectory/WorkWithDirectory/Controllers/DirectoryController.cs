﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WorkWithDirectory.Models;
using System.IO;

namespace WorkWithDirectory.Controllers
{
    public class DirectoryController : ApiController
    {
        // GET: api/Directory
        Items newItem = new Items();
        public Items Get()
        {  
            newItem.SeeDerectory("");
            return newItem;
        }

        // GET: api/Directory/5        
        public IHttpActionResult Get(string path)
        {           
            newItem.SeeDerectory(path);
            return Ok(newItem);
        }

    }
}
