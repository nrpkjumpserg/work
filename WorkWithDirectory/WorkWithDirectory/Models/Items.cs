﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WorkWithDirectory.Models
{     
    public class Items
    {
        public List<string> folders { get; set; }
        public List<string> Lfolders { get; set; }
        public List<string> Lfiles { get; set; }
        public List<string> files { get; set; }
        public int LessTen { get; set; }
        public int TenToFifty { get; set; }
        public int MoreHundred { get; set; }
        public string CurrentDirectory { get; set; }
        int lessFile = 10;
        int midFile = 50;
        int largeFile = 100;
      

        private void Trim()
        {
            foreach (string i in files)
            {                
                    int index = i.LastIndexOf('\\');
                    Lfiles.Add(i.Substring(index + 1, i.Length - index - 1));               
            }
            foreach (string i in folders)
            {
                    int index = i.LastIndexOf('\\');
                    Lfolders.Add(i.Substring(index + 1, i.Length - index - 1));            
            }
        }

       
        //Void to see list of files and folders
        public void SeeDerectory(string path)
        {
            
            folders = new List<string>();
            files = new List<string>();
            Lfolders = new List<string>();
            Lfiles = new List<string>();
            LessTen = 0;
            TenToFifty = 0;
            MoreHundred = 0;
            string slash = "";
            if (path != "")
            {
                slash = "\\";
            }

            if(path.Equals(@"../"))
            {               
                int index = Dir.CDir.LastIndexOf('\\');
                Dir.CDir = Dir.CDir.Substring(0, index);               
                index = Dir.CDir.LastIndexOf('\\');
                if (index != -1)
                {
                    Dir.CDir = Dir.CDir.Substring(0, index);
                }
            }
            else
            {
                Dir.CDir += path;
            }

            Dir.CDir += slash;
            CurrentDirectory = Dir.CDir;
            string[] tmpFiles = Directory.GetFiles(CurrentDirectory);
            string[] tmpDirectory = Directory.GetDirectories(CurrentDirectory);
            foreach (string i in tmpDirectory)
            {
                folders.Add(i);
            }
            foreach (string i in tmpFiles)
            {
                files.Add(i);
            }
            InfoFile();
            Trim();
            
        }
        //Calculate count of files and plus to group counter
        private void InfoFile()
        {
            foreach (string i in files)
            {
                FileInfo f = new FileInfo(i);
                double size = GetSize(f.Length);
                if (size < lessFile)
                { LessTen++; }
                if (size < midFile && size > lessFile)
                { TenToFifty++; }
                if (size > largeFile)
                { MoreHundred++; }
            }
        }
        //File Size in Mb
        private double GetSize(long numb)
        {
            double s = numb / (1024 * 1024);
            return s;
        }
    }
}